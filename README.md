Requisitos:
Visual Studio
Git
Firefox

1. Clonar el repositorio en una carpeta o descargalo como zip
2. Abrir la carpeta con visual studio code
3. En el terminal, en visual studio code, ejecutar los siguientes comandos:
        npm i 
        npm run cypress:runner

4. Para correr toda la prueba sin la interacción gráfica (solo desde consola) --Sin websol
        npm run cypress:execution-sw

5. Para correr toda la prueba sin la interacción gráfica (solo desde consola) --Con websol
        npm run cypress:execution-p

6. Para ejecutar el reporte: 
        npm run cypress:execution-sw

        