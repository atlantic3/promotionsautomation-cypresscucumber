const fs = require('fs-extra');
const report = require('multiple-cucumber-html-reporter');

// Directorio donde se almacenarán los informes
const reportDir = './reports';


// Obtener la fecha y hora actual
const fechaHoraActual = new Date();

// Obtener la fecha actual
const fechaActual = fechaHoraActual.toISOString().split('T')[0];

// Obtener la hora actual
const horaActual = fechaHoraActual.toTimeString().split(' ')[0];



// Borra el directorio existente antes de generar un nuevo informe
async function limpiarDirectorio() {
  try {
    await fs.remove(reportDir);
    console.log('Directorio borrado exitosamente.');
  } catch (err) {
    console.error('Error al borrar el directorio:', err);
  }
}

// Genera el informe después de borrar el directorio
async function generarInforme() {
  await limpiarDirectorio();

  // Configuración del informe
  const config = {
    jsonDir: 'jsonlogs',
    reportPath: `${reportDir}/cucumber-htmlreport.html`,
    displayDuration: true,
    //displayReportTime: true,
    customData: {
      title: 'Run info',
      data: [
          {label: 'Project', value: 'Monitoreo Reset de Promociones'},
          {label: 'Release', value: '4.2.3'},
          {label: 'Hora fin de la prueba', value: `${fechaActual} ${horaActual}`}
      ]
    },
    metadata: {
      browser: {
        name: 'Firefox',
        version: '114',
      },
      device: 'Maquina local',
      platform: {
        name: 'Windows',
        version: '10',
      },
    },
  };

  // Genera el informe
  report.generate(config);
}

// Llama a la función para generar el informe
generarInforme();


