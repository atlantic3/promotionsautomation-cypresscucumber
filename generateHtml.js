const fs = require('fs');
const handlebars = require('handlebars');
const fetch = require('node-fetch');
const path = require('path');



// Función para procesar y mostrar los datos con Handlebars
function mostrarDatos(jsonData) {
    // Compila la plantilla Handlebars
    const source = fs.readFileSync('reporte.handlebars', 'utf-8');
    const template = handlebars.compile(source);

    // Renderiza los datos en el contenedor
    tbody = template({ elementos: jsonData });

    const html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Checklist Tests e2e</title>
        <style>
        body {
            text-align: center;
        }
        h1 {
            text-align: center;
        }
        table {
            width: 70%;
            border-collapse: collapse;
            margin-top: 20px;
            margin: 0 auto;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }

        .circle {
            display: inline-block;
            width: 30px;
            height: 30px;
            border-radius: 50%;
            margin-right: 5px;
        }

        .circle-green {
            background-color: green;
        }

        .circle-red {
            background-color: red;
        }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/handlebars@4.7.7/dist/handlebars.min.js"></script>
    </head>
    <body>
        <h1>Checklist Tests e2e</h1>
        <table id="elementos-table">
            <thead>
                <tr>
                    <th>Validación</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>${tbody}</tbody>
        </table>
        <p>Revisar resultados en: https://dev.azure.com/atlanticcity/Pruebas%202023/_build?definitionId=13&_a=summary</p>
    </body>
    </html>`;

    
// Guarda el HTML estático en un archivo
fs.writeFileSync("output.html", html, err => {
    if (err) console.log(err);
    console.log("File written succesfully");
  });
}

// URL del archivo JSON
const jsonUrl = path.resolve('jsonlogs/log.json');

// Realiza una solicitud fetch para obtener los datos JSON
fs.promises.readFile(jsonUrl, 'utf-8')
    .then(data => JSON.parse(data))
    .then(data => {
        // Verifica si hay algún estado "failed" y actualiza la clase según sea necesario
        data.forEach(item => {
            const hasFailed = Array.isArray(item.elements) && item.elements.some(element => element.steps.some(step => step.result.status === "failed"));
            item.status = hasFailed ? "circle-red" : "circle-green";
        });

        mostrarDatos(data);
    })
    .catch(error => console.error('Error al cargar los datos:', error));

