const puppeteer = require('puppeteer');
const path = require('path');

async function captureScreenshot() {
  const browser = await puppeteer.launch({ headless: 'new' });
  const page = await browser.newPage();

  // Obtén el directorio actual del archivo
const currentDirectory = __dirname;

  // Establecer la resolución de la página
  await page.setViewport({ width: 1920, height: 1080 });

  // Ruta al informe HTML generado por Cucumber
  const reportRelativePath = 'reports/cucumber-htmlreport.html/index.html';

  const reportAbsolutePath = path.join(currentDirectory, reportRelativePath);

  await page.goto(`file://${reportAbsolutePath}`);
  
  // Espera un tiempo para asegurarte de que la página se haya cargado completamente
  await page.waitForTimeout(4000);

  // Toma la captura de pantalla con la resolución configurada
  await page.screenshot({ path: 'screenshot.jpg' });

  await browser.close();
}

// Llama a la función para capturar la pantalla
captureScreenshot();


