const fs = require('fs');
const archiver = require('archiver');

// Ruta de la carpeta que quieres comprimir
const folderToCompress = 'reports';

// Nombre del archivo comprimido
const outputZipPath = 'reporte-e2e.zip';

// Crear un archivo de flujo de escritura para el archivo comprimido
const output = fs.createWriteStream(outputZipPath);

// Crear un objeto Archiver
const archive = archiver('zip', {
  zlib: { level: 9 } // Nivel máximo de compresión
});

// Pipe el archivo de salida al objeto Archiver
archive.pipe(output);

// Agregar la carpeta al archivo comprimido
archive.directory(folderToCompress, false);

// Finalizar el proceso de compresión
archive.finalize();

// Manejar eventos de Archiver
output.on('close', () => {
  console.log(`${outputZipPath} creado con éxito. Tamaño total: ${archive.pointer()} bytes.`);
});

archive.on('error', (err) => {
  throw err;
});
