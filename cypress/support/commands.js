
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import { valores} from './e2e'

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

  Cypress.Commands.add('eliminarModal', () => {
    //elimina modal reto
  cy.get('.MuiDialog-container').should((_) => {}).then(($modal) => {
      if ($modal.length) {
          cy.get('.MuiDialog-container').invoke('remove')
          cy.get('.MuiDialog-root').invoke('remove')
      } else{
          cy.log("NO HAY MODAL")
      }
        })
    
    })
  
  
  Cypress.Commands.add('cambiarid', (idOrigen) => {
    //cambio id
  cy.get('.styles_openButton__oFAO0').should((_) => {}).then(($id) => {
      if ($id.length) {
        cy.get('.styles_openButton__oFAO0').click({force: true})
        cy.get('.styles_boxChangeId__NAAMr > input').clear().type(idOrigen)
        cy.get('.styles_boxChangeId__NAAMr > button').click({force: true})
        cy.wait(3500)
      } else{
          cy.log("NO PERMITE CAMBIAR ID")
      }
        })
    
    })
  
  let isActive
  
  function active (){
    
    cy.get('.btn').should((_) => {}).then(($btn) => {
    if (!$btn.length) {
  
      isActive = 0
      
    } else {
  
      isActive = 1
  
    }
  
    return cy.wrap(isActive).as("activo")
  
  })
  }
  
  Cypress.Commands.add('torneoActivo', active)

  Cypress.Commands.add('obtenerPremio', () =>{

    cy.get('[data-testid="moneyAmountFreeCell"]').should((_) => {}).then(($premio)=> {
  
      if ($premio.length) {
  
      const premioMega = $premio.attr("data-test-value")
      cy.log(premioMega)
      valores.premio= premioMega
      cy.log('ARREGLO:' +     valores[8])
      }else{
          valores.premio= "0"
      }
  })
  
  })
  
  
  Cypress.Commands.add('obtenerPuesto', () => {

    cy.get('#idPuesto').should((_) => {}).then(($btn) => {
        if ($btn.length) {
            cy.get('#idPuesto').then(($puesto)=> {
                const puestoMega = $puesto.text()
                cy.log(puestoMega)
                valores.puesto= puestoMega
                cy.log('ARREGLO:' +     valores[9])
            })
  
     } else {
        cy.get('[data-testid="imageFreeCell"]').then(($puesto)=> {
            const puestoMega = $puesto.attr("data-test-value")
            cy.log(puestoMega)
            valores.puesto= puestoMega
            cy.log('ARREGLO:' +     valores[6])
        })
      }})
    })
  
  Cypress.Commands.add('obtenerPuntos', () =>{

    cy.get('#idPuntos').should((_) => {}).then(($btn) => {
      if ($btn.length) {
          cy.get('#idPuntos').then(($puntos)=> {
              let puntos = $puntos.text()
              cy.log(puntos)
              var puntajeA = puntos.replaceAll(",","")
              puntos = puntajeA
              cy.log(puntajeA)
              cy.log(puntos)
              valores.puntos= puntos
              cy.log('ARREGLO:' +     valores[7])
          })

   } else {
      cy.get('#idpuntaje').then(($puntos)=> {
          let puntos = $puntos.text()
          cy.log(puntos)
          var puntajeA = puntos.replaceAll(",","")
          puntos = puntajeA
          cy.log(puntajeA)
          cy.log(puntos)
          valores.puntos= puntajeA
          cy.log('ARREGLO:' +     valores[7])
      })
    }})
  
  })
  
  
  Cypress.Commands.add('obtenerOpcionesDisp', () =>{
  
    cy.contains("Equivalen a").then(($opcionesdisp)=> {
      const opcdis = $opcionesdisp.attr("data-test-value")
      //cy.writeFile('cypress/fixtures/users.json', {opcdisEstelar: opcdisEstelar})   
      cy.log(opcdis)
      valores.opcionesDisponibles = opcdis
      cy.log('ARREGLO:' +     valores[5])
    })
    
  })
  
  
  Cypress.Commands.add('obtenerCupon', () =>{

    cy.get('[data-testid="opcionesCanjeadas"]').then(($cupon)=> {
      const cupon = $cupon.attr("data-test-value")
      //cy.writeFile('cypress/fixtures/users.json', {cupEstelar: cupEstelar}) 
      cy.log(cupon)
        valores.cupon = cupon
      cy.log('ARREGLO:' +     valores[4])
    })
    
  })
  