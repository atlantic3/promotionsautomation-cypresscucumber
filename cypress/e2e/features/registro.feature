#language: es
  
Característica: Validar flujo de registro
  Antecedentes: 
Dado que la web de registro esté disponible
Escenario:Validar flujo de registro
    Entonces valido que los elementos del paso 1 estén disponibles
    Y ingreso nombres y apellidos
    Y ingreso email
    Y ingreso tipo de documento y documento
    Y ingreso celular
    Y marco los recuadros de condiciones y privacidad
    Y presiono el botón siguiente
    Y valido que los elementos del paso 2 estén disponibles
    Y ingreso mi alias
    Y ingreso mi contraseña
    Y cambio la moneda
    Y selecciono los tipos de bonos