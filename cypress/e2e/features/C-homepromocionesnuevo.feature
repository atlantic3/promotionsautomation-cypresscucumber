#language: es
  
Característica: Validar home de promociones "nuevo"
  Antecedentes: 
    Dado Que la web esté disponible 
  Escenario:Validar home mis promociones
    Cuando  Al dar click en el menu con una cuenta por promociones por activar
    Entonces Ingreso  mi usuario y contraseña con una cuenta por promociones por activar
    Y Me logueo correctamente con una cuenta por promociones por activar

    Entonces ingreso al home de promociones
    Y entro a la  ventana de nuevo
    Y valido el titulo torneo de mesas en nuevo
    Y valido el titulo mega torneo en nuevo
    Y valido el titulo winner de winners en nuevo
    Y valido el titulo torneo de cuotas en nuevo
    Y valido la imagen del card de la promocion drops and wins
    Y valido la imagen del card de la promocion pago anticipado