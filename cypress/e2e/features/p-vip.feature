#language: es
  
Característica: Validar Sorteo Vip
  Antecedentes: 
    Dado Que la web esté disponible
  Escenario:Validar data de Sorteo Vip
    Cuando Me logueo con Testautomation01 
    
    Dado Que la web de Vip esté disponible

    Cuando Cambio el id del invitado

    Entonces Recopilo la data de Sorteo Vip

  Escenario:Validar data de CRM de Sorteo Vip
    Dado Que se haya recopilado correctamente la data

    Entonces Comparo la data web de Sorteo Vip contra CRMCloud

    
  Escenario:Validar data de Websol de Sorteo Vip
    Dado Que se haya recopilado correctamente la data

    Entonces Comparo la data web de Sorteo Vip contra Websol

   