#language: es
  
Característica: Validar lobby de promociones
  Antecedentes: 
    Dado Que la web esté disponible para validar el lobby de promociones
  Escenario:Validar lobby de promociones
    Cuando  Al dar click en el menu
    Y Ingreso  mi usuario y contraseña
    Y Me logueo correctamente
    
    Entonces Encuentro el titulo tus promociones
        
    Entonces Encuentro el titulo de sorteo estelar

    Entonces Encuentro el titulo de mega torneo atlantic

      
    Entonces Encuentro el titulo de top atlantic

    
    Y Encuentro el titulo de sorteo de tus sueños

    
    Y Encuentro el titulo de atlantic vip royal

    
    Y Encuentro el titulo de drops and wins

    
    Y Encuentro el titulo de torneo de cuotas

    
    Y Encuentro el titulo de torneo winner de winners

    
    Y Encuentro el titulo de pago anticipado

    Y Encuentro el titulo de torneo de mesas
