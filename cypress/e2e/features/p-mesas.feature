#language: es
  
Característica: Validar Torneo de Mesas
  Antecedentes: 
    Dado Que la web esté disponible
  Escenario: Validar data de Torneo de Mesas
    Cuando Me logueo con Testautomation02
    
    Dado Que la web de Mesas esté disponible

    Cuando Cambio el id del invitado

    Entonces Recopilo la data de Torneo de Mesas

  Escenario:Validar data de CRM de Torneo de Mesas
    Dado Que se haya recopilado correctamente la data

    Entonces Comparo la data web de Torneo de Mesas contra CRMCloud

