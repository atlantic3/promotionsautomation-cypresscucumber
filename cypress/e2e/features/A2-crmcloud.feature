#language: es
  
Característica: Validar obtención de data de CRMCloud
  Antecedentes: 
    Dado Que CRMCloud esté disponible para revisar la data de promociones
  Escenario:Validar data CRM Cloud
    Cuando  Ingreso  mi usuario y contraseña en CRM
    Entonces Me logueo correctamente en CRM
    
    Dado Busco la tarjeta del invitado
    Entonces Obtengo la data de los sorteos
    Entonces Obtengo la data de los torneos
