#language: es
  
Característica: Validar obtención de data de Websol
  Antecedentes: 
    Dado Que Websol esté disponible para revisar la data de promociones
  Escenario:Validar data de Websol
    Cuando  Ingreso  mi usuario y contraseña en Websol
    Entonces Me logueo correctamente en Websol
      
    Dado Busco la tarjeta del invitado en Websol
    Entonces Obtengo la data de los sorteos y torneos
