#language: es
Característica: Validar Sorteo de tus sueños navidad (FRONT)
Antecedentes: 
    Dado Que la web esté disponible de qa


Escenario:Validar ventana de Informacion
    Cuando  Al dar click en el menu
    Entonces Ingreso  mi usuario y contraseña
    Y Me logueo correctamente
    Entonces Ingreso a la promocion sorteo de tus sueños navidad
    Y Valido el banner de fondo
    Y Valido la imagen de sorteo de tus sueños navideño
    Y Valido el titulo de la promocion
    Y Valido el monto de premio de la promocion
    Y valido el texto del titulo de los multiplicadores  y el monto de multiplicadores
    Y Valido el paso 1 de ¿Comó participar?
    Y Valido el paso 2 de ¿Comó participar?
    Y Valido el paso 3 de ¿Comó participar?
    Y Valido A tener cuenta al acumular puntos
    Y Valido A tener cuenta periodo de acumulacion navideño
    Y Valido A tener cuenta los canjes
    Y Valido A tener cuenta exclusiones de casino online
    Y Valido el titulo de resultados Sorteo Navideño
    Y Valido el subtitulo canjea tus opciones
    Y Valido la fecha del sorteo viernes 29 1:30am
    Y Valido la cantidad de ganadores