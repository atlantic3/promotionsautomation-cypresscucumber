#language: es
  
Característica: Validar Sorteo de tus Sueños
  Antecedentes: 
    Dado Que la web esté disponible
  Escenario:Validar data de Sorteo de tus Sueños
    Cuando Me logueo con Testautomation01 
    
    Dado Que la web de Sueños esté disponible

    Cuando Cambio el id del invitado

    Entonces Recopilo la data de Sorteo de tus Sueños

  Escenario:Validar data de CRM de Sorteo de tus Sueños
    Dado Que se haya recopilado correctamente la data

    Entonces Comparo la data web de Sueños contra CRMCloud

    