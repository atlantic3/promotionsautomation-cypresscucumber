#language: es
  
Característica: Validar Torneo Top
  Antecedentes: 
    Dado Que la web esté disponible
  Escenario:Validar data de torneo Top
   Cuando Me logueo con Testautomation02
    
    Dado Que la web de Top esté disponible

    Cuando Cambio el id del invitado

    Entonces Recopilo la data de Torneo Top

  Escenario:Validar data de CRM de torneo Top
    Dado Que se haya recopilado correctamente la data

    Entonces Comparo la data web de Top contra CRMCloud
