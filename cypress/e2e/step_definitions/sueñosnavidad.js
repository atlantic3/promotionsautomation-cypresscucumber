import {
    Given,
    When,
    Then,
    And,
} 
from "@badeball/cypress-cucumber-preprocessor";
import 'cypress-wait-until';


Given ("Que la web esté disponible de qa", () => {
    cy.visit("https://www.casinoatlanticcity.com/casino-online");
});

Then ("Ingreso a la promocion sorteo de tus sueños navidad", () => {
    cy.visit("https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-suenos-navidad");
});

Then ("Valido el banner de fondo", () => {
// Validar que el elemento existe
cy.get('._header_vpmkr_2').should('exist');
// Validar que el elemento sea visible
cy.get('._header_vpmkr_2').should('be.visible');
// Esperar a que el elemento cargue completamente
cy.get('._header_vpmkr_2').should('be.visible').and('have.css', 'opacity', '1');



});


Then("Valido la imagen de sorteo de tus sueños navideño", () => {
    // Espera explícita antes de interactuar con el elemento
    Then("Valido la imagen de sorteo de tus sueños navideño", () => {
        // cy.wait(2000); // Comenta o elimina esta línea si no es necesaria la espera
        cy.get('.promos-v3-w-[120px]').should('exist').within(() => {
            cy.get('img')
                .should('have.attr', 'src', 'https://ctp-cms.s3.amazonaws.com/CMS_COL/QA/PROMOCIONES/assets/suenosN_logo.svg');
        });
    });
    
});

Then ("Valido el titulo de la promocion", () => {
    cy.get('._header__name_vpmkr_30').should('have.text', 'Sorteo de tus sueños');

});
Then ("Valido el monto de premio de la promocion", () => {
    cy.get('._decorators_knp8h_69 h3').each(($element) => {
        const textoH3 = $element.text().trim();
        cy.log(`Texto de h3: ${textoH3}`);
        
        // Asegura que el texto de h3 sea igual a 'S/75,000S/75,000'
        expect(textoH3).to.equal('S/75,000S/75,000');
        
        // Muestra un mensaje en el log si la comparación es exitosa
        cy.log('La comparación fue exitosa.');
    });
    
    
});

Then ("valido el texto del titulo de los multiplicadores  y el monto de multiplicadores", () => {

        //VALIDAR TITULO
        cy.get('._card__text_70994_26').then(($element) => {
        // Obtiene el texto del elemento
        const textoDelElemento = $element.text().trim();
    
        // Realiza una afirmación para verificar que el texto sea igual a "Aprovecha y multiplica tus puntos:"
        cy.expect(textoDelElemento).to.equal('Aprovecha y multiplica tus puntos:');
        
        //VALIDAR IMAGEN MULTIPLIVADOR X10
        cy.get(':nth-child(1) > ._bubble_117os_16').invoke('attr', 'src').should('eq', 'https://ctp-cms.s3.amazonaws.com/CMS_COL/PROD/PROMOCIONES/assets/multi10x.svg');
        //validar texto del multiplicador que sea el martes
        cy.get(':nth-child(1) > ._multiplier__text_117os_24 > ._multiplier__text__day_117os_30')
        .invoke('text')
        .should('eq', 'Martes');

         //VALIDAR IMAGEN MULTIPLIVADOR X5
        cy.get(':nth-child(2) > ._bubble_117os_16').invoke('attr', 'src').should('eq', 'https://ctp-cms.s3.amazonaws.com/CMS_COL/PROD/PROMOCIONES/assets/multi5x.svg');
         //validar texto del multiplicador que sea el Jueves
        cy.get(':nth-child(2) > ._multiplier__text_117os_24 > ._multiplier__text__day_117os_30')
        .invoke('text')
        .should('eq', 'Jueves');

    });
    
});

Then ("Valido el paso 1 de ¿Comó participar?", () => {
    cy.get(':nth-child(1) > ._box__item-description_10l76_26')
    .invoke('text')
    .should('eq', 'Juega y acumula puntos en tus máquinas favoritas.');
    
});
Then ("Valido el paso 2 de ¿Comó participar?", () => {
    cy.get('._box__item-description_10l76_26 > strong')
    .invoke('html')  // Cambiado a 'html' en lugar de 'text'
    .should('eq', 'el jueves 28 de diciembre.');
    
});
    
    

Then ("Valido el paso 3 de ¿Comó participar?", () => {
    cy.get(':nth-child(3) > ._box__item-description_10l76_26')
    .invoke('text')
    .should('eq', 'Recuerda que debes jugar el día de canje para que tus opciones sean válidas.');
    
    
});

Then ("Valido A tener cuenta al acumular puntos", () => {
    cy.contains('Acumula puntos: solo con tu saldo efectivo. El saldo bono no cuenta.')
});
Then ("Valido A tener cuenta periodo de acumulacion navideño", () => {
    cy.get('.promos-v3-list-disc > :nth-child(2) > p.promos-v3-text-base')
    .invoke('text')
    .should('eq', 'Periodo de acumulación sorteo navideño: Viernes 01 de diciembre a las 00:00 hrs hasta el jueves 28 de diciembre a las 23:59 hrs.');
    
    
});
Then ("Valido A tener cuenta los canjes", () => {
    cy.contains('Canje: Mínimo es de 2,000 opciones y máximo de 20,000 opciones.');
    
    
});
Then ("Valido A tener cuenta exclusiones de casino online", () => {

    cy.contains('Exclusiones casino online: Juegos de póker, ruleta, juegos de mesa, mesas en vivo, apuestas deportivas y deportes virtuales.');
    
    
});
Then ("Valido el titulo de resultados Sorteo Navideño", () => {

    cy.contains('Sorteo navideño');
    
    
});
Then ("Valido el subtitulo canjea tus opciones", () => {
    cy.contains('Canjea tus opciones:');
    cy.contains('Jueves 28 de diciembre');
    
    
});
Then ("Valido la fecha del sorteo viernes 29 1:30am", () => {
    cy.contains('Viernes 29 a las 1:30 AM');
    
    
});
Then ("Valido la cantidad de ganadores", () => {
    cy.contains('36 ganadores');
    
    
});