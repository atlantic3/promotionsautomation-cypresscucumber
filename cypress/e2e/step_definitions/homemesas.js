import {
    Given,
    When,
    Then,
    And,
} 
from "@badeball/cypress-cucumber-preprocessor";
import 'cypress-wait-until';

Then ("Valido la imagen del banner de mesas", () => {
    cy.wait(5000)
    cy.get('.fondoimgBanner')
        .should('exist')  
        .should('be.visible');  

    
});

Then ("Valido el titulo del banner de mesas", () => {
    cy.get('.bannerTitle').should('be.visible');
    cy.get('.bannerTitle').should('have.text', 'MESAS');
});

Then ("Valido el boton favoritos de mesas", () => {
    cy.get('form > .clmc-btn-primary')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/favoritos.svg');

});

Then ("Valido la categoria de ruleta en mesas", () => {
    cy.get('#menu_item_ruleta > .casino-menu-item')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/ruleta.svg');
});

Then ("Valido la categoria de blackjack en mesas", () => {
    cy.get('#menu_item_blackjack > .casino-menu-item')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/blackjack.svg');
});

Then ("Valido la categoria de poker en mesas", () => {
    cy.get('#menu_item_poker > .casino-menu-item')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/poker.svg');
});
Then ("Valido la categoria de baccarat en mesas", () => {
    cy.get('#menu_item_baccarat > .casino-menu-item')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/bacarat.svg');
});
Then ("Valido la categoria de todos los juegos en mesas", () => {
    cy.get('#menu_item_todos > .casino-menu-item')
        .find('img')  // Busca la etiqueta de imagen dentro del botón
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/todos.svg');
});
Then ("Valido la seccion de ruleta en mesas", () => {
    //VALIDO LA IMAGEN
    cy.get(':nth-child(2) > .sliderLobby-header > :nth-child(1) > img')
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/title/ruleta.svg');

    //VALIDO EL TEXTO
    cy.get(':nth-child(2) > .sliderLobby-header > :nth-child(1) > p')
        .should('have.text', 'Ruleta');
    //VALIDO VER MAS
    cy.get(':nth-child(2) > .sliderLobby-header > a > .clmc-btn-secondary')
        .should('exist')  // Asegúrate de que el elemento exista en la página
        .should('have.text', 'VER MÁS');  // Asegúrate de que el texto del elemento sea "Ver más"

    //VALIDO JUEGOS QUE EXISTAN    
    cy.get('#sliderLobby-1 > .sliderLobbyRow1 > :nth-child(1) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-1 > .sliderLobbyRow1 > :nth-child(2) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-1 > .sliderLobbyRow1 > :nth-child(3) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-1 > .sliderLobbyRow1 > :nth-child(4) > .styles_cover__Kx2nX')
    .should('exist');

});

Then ("Valido la seccion de blackjack en mesas", () => {
    //VALIDO LA IMAGEN
    cy.get(':nth-child(3) > .sliderLobby-header > :nth-child(1) > img')
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/title/blackjack.svg');

    //VALIDO EL TEXTO
    cy.get(':nth-child(3) > .sliderLobby-header > :nth-child(1) > p')
        .should('have.text', 'Blackjack');
    //VALIDO VER MAS
    cy.get(':nth-child(3) > .sliderLobby-header > a > .clmc-btn-secondary')
        .should('exist')  // Asegúrate de que el elemento exista en la página
        .should('have.text', 'VER MÁS');  // Asegúrate de que el texto del elemento sea "Ver más"

    //VALIDO JUEGOS QUE EXISTAN    
    cy.get('#sliderLobby-2 > .sliderLobbyRow1 > :nth-child(1) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-2 > .sliderLobbyRow1 > :nth-child(2) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-2 > .sliderLobbyRow1 > :nth-child(3) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-2 > .sliderLobbyRow1 > :nth-child(4) > .styles_cover__Kx2nX')
    .should('exist');

});

Then ("Valido la seccion de poker en mesas", () => {
    //VALIDO LA IMAGEN
    cy.get(':nth-child(4) > .sliderLobby-header > :nth-child(1) > img')
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/title/poker.svg');

    //VALIDO EL TEXTO
    cy.get(':nth-child(4) > .sliderLobby-header > :nth-child(1) > p')
        .should('have.text', 'Poker');
    

    //VALIDO JUEGOS QUE EXISTAN    
    cy.get('#sliderLobby-3 > .sliderLobbyRow1 > :nth-child(1) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-3 > .sliderLobbyRow1 > :nth-child(2) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-3 > .sliderLobbyRow1 > :nth-child(3) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-3 > .sliderLobbyRow1 > :nth-child(4) > .styles_cover__Kx2nX')
    .should('exist');

});

Then ("Valido la seccion de baccarat en mesas", () => {
    //VALIDO LA IMAGEN
    cy.get(':nth-child(5) > .sliderLobby-header > :nth-child(1) > img')
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/title/bacarat.svg');

    //VALIDO EL TEXTO
    cy.get(':nth-child(5) > .sliderLobby-header > :nth-child(1) > p')
        .should('have.text', 'Baccarat');
    

    //VALIDO JUEGOS QUE EXISTAN    
    cy.get('#sliderLobby-4 > .sliderLobbyRow1 > :nth-child(1) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-4 > .sliderLobbyRow1 > :nth-child(2) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-4 > .sliderLobbyRow1 > :nth-child(3) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-4 > .sliderLobbyRow1 > :nth-child(4) > .styles_cover__Kx2nX')
    .should('exist');

});

Then ("Valido la seccion de todo los juegos en mesas", () => {
    //VALIDO LA IMAGEN
    cy.get(':nth-child(6) > .sliderLobby-header > :nth-child(1) > img')
        .should('have.attr', 'src', 'https://www.casinoatlanticcity.com/static/img/casino-online-menu/title/todos.svg');

    //VALIDO EL TEXTO
    cy.get(':nth-child(6) > .sliderLobby-header > :nth-child(1) > p')
        .should('have.text', 'Todos los juegos');
    //VALIDO VER MAS
    cy.get(':nth-child(6) > .sliderLobby-header > a > .clmc-btn-secondary')
        .should('exist')  // Asegúrate de que el elemento exista en la página
        .should('have.text', 'VER MÁS');  // Asegúrate de que el texto del elemento sea "Ver más"

    //VALIDO JUEGOS QUE EXISTAN    
    cy.get('#sliderLobby-5 > .sliderLobbyRow1 > :nth-child(1) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-5 > .sliderLobbyRow1 > :nth-child(2) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-5 > .sliderLobbyRow1 > :nth-child(3) > .styles_cover__Kx2nX')
    .should('exist');

    cy.get('#sliderLobby-5 > .sliderLobbyRow1 > :nth-child(4) > .styles_cover__Kx2nX')
    .should('exist');

});







/*Escenario:Validar HOME COL
    Cuando  Al dar click en el menu
    Entonces Ingreso  mi usuario y contraseña
    Y Me logueo correctamente
    


Escenario:Validar HOME MAQUINAS
    Cuando  Al dar click en el menu
    Entonces Ingreso  mi usuario y contraseña
    Y Me logueo correctamente
    Cuando Al dar click en el menu hamburguesa
    Entonces Muestra toda las opciones del menu hamburguesa
    Entonces Ingreso al HOME MAQUINAS
    Y Valido la imagen del banner
    Y Valido el titulo del banner
    Y valido el*///