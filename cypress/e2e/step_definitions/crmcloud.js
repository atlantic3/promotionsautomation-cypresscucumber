import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';
  import { credentials, invitado} from '../../support/e2e.js'

  const valoresCrm = []



  
  Given ("Que CRMCloud esté disponible para revisar la data de promociones", () => {
    cy.visit('https://crmatlantic.acity.com.pe/')
  });

  When ("Ingreso  mi usuario y contraseña en CRM", () => {
  
    cy.get('#username').type(credentials.user)
    cy.get('#password').type(credentials.pass)   
  });
  
  
  Then ("Me logueo correctamente en CRM", () => {
    cy.get('.form-actions > .btn').click()
    cy.get(':nth-child(1) > .dropdown-toggle > .fa').click()
   
  });
  
  Given ("Busco la tarjeta del invitado",()=>{

    cy.get('#txtTarjeta').type(invitado.cuenta)
    cy.get('#btnBuscarLista').click()
    cy.wait(4000)
    cy.get('.red').click()
    cy.wait(7000)

  });   



  Then ("Obtengo la data de los sorteos",()=>{

    // Seleccionar la tabla por su ID
    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
        // Utilizar el método each() para recorrer las filas
        cy.get('tr').each(($fila) => {
          // Buscar el texto deseado en la primera columna
          if ($fila.find('td:eq(0)').text().includes('Sorteo estelar Avance')) {

            // Obtener el texto del elemento en la segunda columna
            const textoColumna1 = $fila.find('td:eq(0)').text();

            // Obtener el texto del elemento en la segunda columna
            const textoColumna2 = $fila.find('td:eq(1)').text();
            
            // Imprimir la clase y el texto en la consola
            cy.log(`El texto de la columna 1: ${textoColumna1}`);
            cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
            
            // promo
            const promo1 = textoColumna1.replace(/\t/g, '')
            const promo2 = promo1.split('\n')
            const promo = promo2[2]
            let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
            premio  = premio .replace(",","")
            //valoresCrm.push({promo, premio})
            const opcionesDisponibles   = textoColumna2.split(/(\d+(?!\.))/)[3]
            //valoresCrm.push({promo, textoColumna2})
            const cupones   = textoColumna2.split(/(\d+(?!\.))/)[5]
            //valoresCrm.push({promo, cupones})
            const puntos   = textoColumna2.split(/(\d+(?!\.))/)[7]
            //valoresCrm.push({promo, puntos}) 
    
            let sorteo  = {promo, premio , opcionesDisponibles , cupones , puntos }
            valoresCrm.push(sorteo )    

            cy.log('arregloTotal',sorteo )
            cy.log("DATA", premio , opcionesDisponibles , cupones , puntos )
            cy.log('ARREGLO:' + valoresCrm.length)

          }
        });
      });


    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
        // Utilizar el método each() para recorrer las filas
        cy.get('tr').each(($fila) => {
          // Buscar el texto deseado en la primera columna
          if ($fila.find('td:eq(0)').text().includes('Atlantic VIP Avance')) {

            // Obtener el texto del elemento en la segunda columna
            const textoColumna1 = $fila.find('td:eq(0)').text();

            // Obtener el texto del elemento en la segunda columna
            const textoColumna2 = $fila.find('td:eq(1)').text();
            
            // Imprimir la clase y el texto en la consola
            cy.log(`El texto de la columna 1: ${textoColumna1}`);
            cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
            
            // promo
            const promo1 = textoColumna1.replace(/\t/g, '')
            const promo2 = promo1.split('\n')
            const promo = promo2[2]
            let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
            premio  = premio .replace(",","")
            //valoresCrm.push({promo, premio})
            const opcionesDisponibles   = textoColumna2.split(/(\d+(?!\.))/)[3]
            //valoresCrm.push({promo, textoColumna2})
            const cupones   = textoColumna2.split(/(\d+(?!\.))/)[5]
            //valoresCrm.push({promo, cupones})
            const puntos   = textoColumna2.split(/(\d+(?!\.))/)[7]
            //valoresCrm.push({promo, puntos}) 
    
            let sorteo  = {promo, premio , opcionesDisponibles , cupones , puntos }
            valoresCrm.push(sorteo )    

            cy.log('arregloTotal',sorteo )
            cy.log("DATA", premio , opcionesDisponibles , cupones , puntos )
            cy.log('ARREGLO:' + valoresCrm.length)

          }
        });
      });


    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
        // Utilizar el método each() para recorrer las filas
        cy.get('tr').each(($fila) => {
          // Buscar el texto deseado en la primera columna
          if ($fila.find('td:eq(0)').text().includes('Sorteo de tus Sueños Avance')) {

            // Obtener el texto del elemento en la segunda columna
            const textoColumna1 = $fila.find('td:eq(0)').text();

            // Obtener el texto del elemento en la segunda columna
            const textoColumna2 = $fila.find('td:eq(1)').text();
            
            // Imprimir la clase y el texto en la consola
            cy.log(`El texto de la columna 1: ${textoColumna1}`);
            cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
            
            // promo
            const promo1 = textoColumna1.replace(/\t/g, '')
            const promo2 = promo1.split('\n')
            const promo = promo2[2]
            let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
            premio  = premio .replace(",","")
            //valoresCrm.push({promo, premio})
            const opcionesDisponibles   = textoColumna2.split(/(\d+(?!\.))/)[3]
            //valoresCrm.push({promo, textoColumna2})
            const cupones   = textoColumna2.split(/(\d+(?!\.))/)[5]
            //valoresCrm.push({promo, cupones})
            const puntos   = textoColumna2.split(/(\d+(?!\.))/)[7]
            //valoresCrm.push({promo, puntos}) 
    
            let sorteo  = {promo, premio , opcionesDisponibles , cupones , puntos }
            valoresCrm.push(sorteo )    

            cy.log('arregloTotal',sorteo )
            cy.log("DATA", premio , opcionesDisponibles , cupones , puntos )
            cy.log('ARREGLO:' + valoresCrm.length)

          }
        });
      });


  });


  Then ("Obtengo la data de los torneos",()=>{

    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
            // Utilizar el método each() para recorrer las filas
            cy.get('tr').each(($fila) => {
              // Buscar el texto deseado en la primera columna
              if ($fila.find('td:eq(0)').text().includes('Top Atlantic Avance')) {
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna1 = $fila.find('td:eq(0)').text();
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna2 = $fila.find('td:eq(1)').text();
                
                // Imprimir la clase y el texto en la consola
                cy.log(`El texto de la columna 1: ${textoColumna1}`);
                cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
                
                // promo
                const promo1 = textoColumna1.replace(/\t/g, '')
                const promo2 = promo1.split('\n')
                const promo = promo2[2]
                let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
                premio  = premio .replace(",","")
                //valoresCrm.push({promo, premio})
                const puesto   = textoColumna2.split(/(\d+(?!\.))/)[3]
                //valoresCrm.push({promo, textoColumna2})
                const puntos   = textoColumna2.split(/(\d+(?!\.))/)[5]
                //valoresCrm.push({promo, cupones})
        
                let sorteo  = {promo, premio , puesto , puntos }
                valoresCrm.push(sorteo )    
    
                cy.log('arregloTotal',sorteo )
                cy.log("DATA", premio , premio , puesto , puntos)
                cy.log('ARREGLO:' + valoresCrm.length)
    
              }
            });
        });
    
    
    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
            // Utilizar el método each() para recorrer las filas
            cy.get('tr').each(($fila) => {
              // Buscar el texto deseado en la primera columna
              if ($fila.find('td:eq(0)').text().includes('Mega Torneo Avance')) {
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna1 = $fila.find('td:eq(0)').text();
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna2 = $fila.find('td:eq(1)').text();
                
                // Imprimir la clase y el texto en la consola
                cy.log(`El texto de la columna 1: ${textoColumna1}`);
                cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
                
                // promo
                const promo1 = textoColumna1.replace(/\t/g, '')
                const promo2 = promo1.split('\n')
                const promo = promo2[2]

                if(textoColumna2.includes('POR ACTIVAR')){

                    var estado = "Inactivo"
                    cy.log(estado)
                    
                    let torneoA = {promo, estado}
                    valoresCrm.push(torneoA)
                    cy.log(valoresCrm)

                }
                else{
                
                let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
                premio  = premio .replace(",","")
                //valoresCrm.push({promo, premio})
                const puesto   = textoColumna2.split(/(\d+(?!\.))/)[3]
                //valoresCrm.push({promo, textoColumna2})
                const puntos   = textoColumna2.split(/(\d+(?!\.))/)[5]
                //valoresCrm.push({promo, cupones})
        
                let sorteo  = {promo, premio , puesto , puntos }
                valoresCrm.push(sorteo )    
    
                cy.log('arregloTotal',sorteo )
                cy.log("DATA", premio , puesto , puntos )
                cy.log('ARREGLO:' + valoresCrm.length)
                }
              }
            });
        });
    
    
    cy.get('.col-md-5 > .portlet > .portlet-body').within(() => {
            // Utilizar el método each() para recorrer las filas
            cy.get('tr').each(($fila) => {
              // Buscar el texto deseado en la primera columna
              if ($fila.find('td:eq(0)').text().includes('Torneo de Mesas') && !$fila.find('td:eq(0)').text().includes('Final')) {
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna1 = $fila.find('td:eq(0)').text();
    
                // Obtener el texto del elemento en la segunda columna
                const textoColumna2 = $fila.find('td:eq(1)').text();
                
                // Imprimir la clase y el texto en la consola
                cy.log(`El texto de la columna 1: ${textoColumna1}`);
                cy.log(`El texto de la columna 2 es: ${textoColumna2}`);
                // promo
                const promo1 = textoColumna1.replace(/\t/g, '')
                const promo2 = promo1.split('\n')
                const promo = promo2[2]

                if(textoColumna2.includes('POR ACTIVAR')){

                    var estado = "Inactivo"
                    cy.log(estado)
                    
                    
                    let torneoA = {promo, estado}
                    valoresCrm.push(torneoA)
                    cy.log(valoresCrm)

                }
                else{
                

                let premio   = textoColumna2.split(/(\d+(,\d+)?)/)[1].toString()
                premio  = premio .replace(",","")
                //valoresCrm.push({promo, premio})
                const puesto   = textoColumna2.split(/(\d+(?!\.))/)[3]
                //valoresCrm.push({promo, textoColumna2})
                const puntos   = textoColumna2.split(/(\d+(?!\.))/)[5]
                //valoresCrm.push({promo, cupones})

        
                let sorteo  = {promo, premio, puesto, puntos }
                valoresCrm.push(sorteo )    
    
                cy.log('arregloTotal',sorteo )
                cy.log("DATA", premio , puesto, puntos )
                cy.log('ARREGLO:' + valoresCrm.length)
    
              }
            }
            });
        });


        cy.writeFile('cypress/fixtures/dataCRM.json', valoresCrm)
        cy.writeFile('cypress/fixtures/dataCRM.txt', valoresCrm)  
          

  });
