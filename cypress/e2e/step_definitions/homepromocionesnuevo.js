import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';



When ("Al dar click en el menu con una cuenta por promociones por activar", () => {
  
    cy.get('.LoginAsideHeaderButtons_themed-login-buttons__Wi6TW > .styles_primaryButton__02bqI').click({ force: true })    
});
  
  
Then ("Ingreso  mi usuario y contraseña con una cuenta por promociones por activar", () => {
    cy.get('#user').type("Testcalimaco51")
    cy.get('#password').type("Testcalimaco51")
  });
  
Then ("Me logueo correctamente con una cuenta por promociones por activar", () => {
    cy.get('.clmc-dark-form > .clmc-btn-primary').click()
    cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
    cy.wait(5000)
  });  

Then ("entro a la  ventana de nuevo", () => {
    cy.get('.styles_sectionsNav__yacy2 > :nth-child(2)').click()
    cy.wait(8000)
});



Then ("valido el titulo torneo de mesas en nuevo", () => {
    cy.wait(3000)
    cy.contains("Torneo de Mesas")
});
Then ("valido el titulo mega torneo en nuevo", () => {
    cy.wait(3000)
    cy.contains("Mega Torneo Atlantic")
});
Then ("valido el titulo winner de winners en nuevo", () => {
    cy.wait(3000)
    cy.contains("Torneo de Cuotas")
});
Then ("valido el titulo torneo de cuotas en nuevo", () => {
    cy.wait(3000)
    cy.contains("Winner de Winners")
});
Then ("valido la imagen del card de la promocion drops and wins", () => {
    cy.get('.styles_sectionsNav__yacy2 > :nth-child(2)').click()
    cy.get(':nth-child(5) > .flip_frontContainer__99Imr > .styles_content__PLJ6H > .styles_noFamilyCard_container__mSYEl > .styles_noFamilyCard_img__SOHbt')
    .invoke('attr', 'src')
    .then((src) => {
        /* 'src' ahora contiene la URL de la imagen
        // Comparar con la URL deseada*/
    cy.log('La fuente de la imagen es:', src);
    expect(src).to.eq('https://www.casinoatlanticcity.com/static/img/promociones/home-trx/cards/bg/drops_wins.png');
    })
});
Then ("valido la imagen del card de la promocion pago anticipado", () => {
    cy.contains("Pago Anticipado")
    cy.get(':nth-child(6) > .flip_frontContainer__99Imr > .styles_content__PLJ6H > .styles_noFamilyCard_container__mSYEl > .styles_noFamilyCard_img__SOHbt')
    .invoke('attr', 'src')
    .then((src) => {
/* 'src' ahora contiene la URL de la imagen
            // Comparar con la URL deseada*/
        cy.log('La fuente de la imagen es:', src);
        expect(src).to.eq('https://www.casinoatlanticcity.com/static/img/promociones/home-trx/cards/bg/pago_anticipado.png');
        });    
});