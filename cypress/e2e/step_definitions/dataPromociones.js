import {
  Given,
  When,
  Then,
  And,
} 
from "@badeball/cypress-cucumber-preprocessor";
import 'cypress-wait-until';
import { credentials, invitado, valores} from '../../support/e2e.js' 


Given ("Que la web esté disponible", () => {
  cy.visit("https://www.casinoatlanticcity.com/casino-online");
});

When ("Me logueo con Testautomation01", () => {
  
cy.get('.LoginAsideHeaderButtons_themed-login-buttons__Wi6TW > .styles_primaryButton__02bqI').click({ force: true })    

  cy.get('#user').type(credentials.usrweb[0])
  cy.get('#password').type(credentials.usrweb[0])

  cy.get('.clmc-dark-form > .clmc-btn-primary').click()
  cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
  cy.wait(5000)
});

When ("Me logueo con Testautomation02", () => {
  
  cy.get('.LoginAsideHeaderButtons_themed-login-buttons__Wi6TW > .styles_primaryButton__02bqI').click({ force: true })    

  cy.get('#user').type(credentials.usrweb[1])
  cy.get('#password').type(credentials.usrweb[1])

  cy.get('.clmc-dark-form > .clmc-btn-primary').click()
  cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
  cy.wait(5000)
});

Given ("Que la web de Top esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/top-atlantic')
  cy.wait(3500)
  
});

When ("Cambio el id del invitado", () => {
 cy.cambiarid(invitado.idOrigen)
 cy.wait(3500)
  
});

Then ("Recopilo la data de Torneo Top", () => {
  cy.wait(3500)
  cy.eliminarModal()
  cy.wait(2500)

  //cambio ID
  cy.cambiarid(invitado.idOrigen)

  cy.contains('Avance').click()


  //Extraer data
  cy.wait(4500) 
  cy.eliminarModal()


cy.contains("¡Empieza a jugar!").should((_) => {}).then(($a) => {
    if(!$a.length){

      cy.obtenerPuesto()
      cy.obtenerPuntos()
      cy.obtenerPremio()
        cy.log(valores)
}else{
    valores.premio = "0"
    valores.puntos = "0"
    valores.puesto = "0"
    cy.log(valores)

    cy.log("Usuario sin puntos")
}
})  
});


Given ("Que se haya recopilado correctamente la data", () => {
  cy.wait(3500)
  expect(Object.values(valores)).to.have.length.greaterThan(0);

 })

Then ("Comparo la data web de Top contra CRMCloud", () => {

  cy.wait(3500)
    cy.readFile('cypress/fixtures/dataCRM.json').its('[3].puntos').should('eq', valores.puntos )
    cy.readFile('cypress/fixtures/dataCRM.json').its('[3].premio').should('eq', valores.premio )
    if (valores.puesto !="-"){
    cy.readFile('cypress/fixtures/dataCRM.json').its('[3].puesto').should('eq', valores.puesto )  
    }
 
});

Then ("Comparo la data web de Top contra Websol", () => {
 // Cargar el archivo JSON utilizando cy.fixture()
 cy.wait(3500)
 cy.fixture('dataWebsol.json').then((datos) => {
  // Verificar si el elemento existe en el objeto JSON
  if (datos[0].premiotop) {
    // Hacer algo si el elemento existe
    cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].premiotop').should('eq', valores.premio )
    cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].puestotop').should('eq', valores.puesto )

  } else {
    // Hacer algo si el elemento no existe
    cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].premiotop').should('eq', valores.premio )
    cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].puestotop').should('eq', valores.puesto )
  }
});

});


Given ("Que la web de Mega esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/mega-torneo')
  cy.wait(3500)
  
});

Then ("Recopilo la data de Megatorneo", () => {
  cy.wait(3500)
  cy.eliminarModal()
  cy.wait(2500)


  cy.contains('Avance').click()


  //Extraer data
  cy.wait(4500) 
  cy.eliminarModal()


cy.contains("¡Empieza a jugar!").should((_) => {}).then(($a) => {
    if(!$a.length){

      cy.torneoActivo().then(activo=> {
        cy.log(activo)
        cy.wait(3000)
        
        if (activo == 0){

            cy.obtenerPuesto()
            cy.obtenerPuntos()
            cy.obtenerPremio()
            
        } else{ 
            cy.log("TORNEO NO ACTIVO")
            valores.torneoMega= "TORNEO INACTIVO"
        }
        })  
}else{
    valores.premio = "0"
    valores.puntos = "0"
    valores.puesto = "0"

    cy.log("Usuario sin puntos")
}
})  
});


Then ("Comparo la data web de Megatorneo contra CRMCloud", () => {


  cy.wait(3500)
  cy.readFile('cypress/fixtures/dataCRM.json').its('[4].puntos').should('eq', valores.puntos )
  cy.readFile('cypress/fixtures/dataCRM.json').its('[4].premio').should('eq', valores.premio )
  if (valores.puesto !="-"){
  cy.readFile('cypress/fixtures/dataCRM.json').its('[4].puesto').should('eq', valores.puesto )  
  }
  

});

Then ("Comparo la data web de Megatorneo contra Websol", () => {
  cy.wait(3500)
  cy.fixture('dataWebsol.json').then((datos) => {
    // Verificar si el elemento existe en el objeto JSON
    if (datos[0].premiomega) {
      // Hacer algo si el elemento existe
      cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].premiomega').should('eq', valores.premio )
      cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].puestomega').should('eq', valores.puesto )

    } else {
      // Hacer algo si el elemento no existe
      cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].premiomega').should('eq', valores.premio )
      cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].puestomega').should('eq', valores.puesto )
    }
  });


});

Given ("Que la web de Mesas esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/torneo-mesas')
  cy.wait(3500)
  
});

Then ("Recopilo la data de Torneo de Mesas", () => {
  cy.wait(3500)
  cy.eliminarModal()
  cy.contains('Avance').click()
  cy.wait(12000)

  cy.contains("¡Empieza a jugar!").should((_) => {}).then(($a) => {
      if(!$a.length){

      cy.torneoActivo().then(activo=> {
          cy.log(activo)
          cy.wait(3000)
          
          if (activo == 0){

              cy.obtenerPuesto()
              cy.obtenerPuntos()
              cy.obtenerPremio()
              
          } else{ 
              cy.log("TORNEO NO ACTIVO")
              valores.torneoMesas= "TORNEO INACTIVO"
              valores.premio = "0"
              valores.puntos = "0"
              valores.puesto = "0"
          }
          })  

  } else {

      valores.premio = "0"
      valores.puntos = "0"
      valores.puesto = "0"
      cy.log("Usuario sin puntos")

  }

  })

});


Then ("Comparo la data web de Torneo de Mesas contra CRMCloud", () => {
  cy.wait(3500)
  if(valores.torneoMesas !=  "TORNEO INACTIVO"){

    cy.readFile('cypress/fixtures/dataCRM.json').its('[5].puntos').should('eq', valores.puntos )
    cy.readFile('cypress/fixtures/dataCRM.json').its('[5].premio').should('eq', valores.premio )
    if (valores.puesto !="-"){
    cy.readFile('cypress/fixtures/dataCRM.json').its('[5].puesto').should('eq', valores.puesto )  
    }


  }

  
});


Given ("Que la web de Vip esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-vip-royal')
  cy.wait(3500)
  
});

Then ("Recopilo la data de Sorteo Vip", () => {
  cy.wait(3500)
      cy.eliminarModal()
      cy.contains('Canje').click()
      cy.wait(4000)

      cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
      if (!$btn.length) {
          cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
              if (!$btn2.length) {
          
          cy.obtenerPuntos()
          cy.obtenerCupon()
          cy.obtenerOpcionesDisp()
          }    else{
              cy.obtenerCupon()
              valores.puntos= "0"
              valores.opcionesDisponibles= "0"

              }
          })
      } else{
          cy.obtenerCupon()
              valores.puntos= "0"
              valores.opcionesDisponibles= "0"
      }

      })

}); 

Then ("Comparo la data web de Sorteo Vip contra CRMCloud", () => {
cy.wait(3500)
cy.readFile('cypress/fixtures/dataCRM.json').its('[1].puntos').should('eq', valores.puntos )
cy.readFile('cypress/fixtures/dataCRM.json').its('[1].opcionesDisponibles').should('eq', valores.opcionesDisponibles )  
cy.readFile('cypress/fixtures/dataCRM.json').its('[1].cupones').should('eq', valores.cupon )  

});

Then ("Comparo la data web de Sorteo Vip contra Websol", () => {
  cy.wait(3500)
    // Cargar el archivo JSON utilizando cy.fixture()
    cy.fixture('dataWebsol.json').then((datos) => {
      // Verificar si el elemento existe en el objeto JSON
      if (datos[0].opcdispovip) {
        // Hacer algo si el elemento existe
        cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].opcdispovip').should('eq', valores.opcionesDisponibles )
      } else {
        // Hacer algo si el elemento no existe
        cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].opcdispovip').should('eq', valores.opcionesDisponibles )
      }
    });

});


Given ("Que la web de Estelar esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-estelar')
  cy.wait(3500)
  
});

Then ("Recopilo la data de Sorteo Estelar", () => {
  cy.wait(3500)
  cy.eliminarModal()
  cy.wait(5000)
  cy.contains('Canje').click()

  cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
      if (!$btn.length) {
          cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
          if (!$btn2.length) {
              cy.obtenerPuntos()
              cy.obtenerCupon()
              cy.obtenerOpcionesDisp()
              cy.log(valores)
              }
              else{
                  cy.obtenerCupon()
                      valores.opcionesDisponibles = "0"
                      valores.puntos = "0"    
                  
              }
          })
      }
      else{
          cy.obtenerCupon()
              valores.opcionesDisponibles = "0"
              valores.puntos = "0"
          
  
      }
  
  })

      
     
});

Then ("Comparo la data web de Sorteo Estelar contra CRMCloud", () => {
  cy.wait(3500)
  cy.readFile('cypress/fixtures/dataCRM.json').its('[0].puntos').should('eq', valores.puntos )
  cy.readFile('cypress/fixtures/dataCRM.json').its('[0].opcionesDisponibles').should('eq', valores.opcionesDisponibles )
  cy.readFile('cypress/fixtures/dataCRM.json').its('[0].cupones').should('eq', valores.cupon )  

});

Then ("Comparo la data web de Sorteo Estelar contra Websol", () => {
  cy.wait(3500)
      // Cargar el archivo JSON utilizando cy.fixture()
      cy.fixture('dataWebsol.json').then((datos) => {
        // Verificar si el elemento existe en el objeto JSON
        if (datos[0].opcdispovi) {
          // Hacer algo si el elemento existe
          cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].opcdispest').should('eq', valores.opcionesDisponibles )
        } else {
          // Hacer algo si el elemento no existe
          cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].opcdispest').should('eq', valores.opcionesDisponibles )
        }
      });

});

Given ("Que la web de Sueños esté disponible", () => {
  cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-suenos')
  cy.wait(3500)
  
});

Then ("Recopilo la data de Sorteo de tus Sueños", () => {
  cy.wait(3500)
  cy.eliminarModal()
  cy.wait(4000)
  cy.contains('Canje').click()
  cy.wait(4000)

  cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
      if (!$btn.length) {
          cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
          if (!$btn2.length) {
              cy.obtenerPuntos()
              cy.obtenerCupon()
             cy.obtenerOpcionesDisp()
          }
          else{
              cy.obtenerCupon()
              valores.opcionesDisponibles= "0"
              valores.puntosSueños= puntos= "0"

          
       }})

      }  else{
              cy.obtenerCupon()
              valores.opcionesDisponibles= "0"
              valores.puntosSueños= puntos= "0"
          
       }
  });

  });

  Then ("Comparo la data web de Sueños contra Websol", () => {
    cy.wait(3500)
    // Cargar el archivo JSON utilizando cy.fixture()
    cy.fixture('dataWebsol.json').then((datos) => {
      // Verificar si el elemento existe en el objeto JSON
      if (datos[0].opcdispsue) {
        // Hacer algo si el elemento existe
        cy.readFile('cypress/fixtures/dataWebsol.json').its('[0].opcdispsue').should('eq', valores.opcionesDisponibles )
      } else {
        // Hacer algo si el elemento no existe
        cy.readFile('cypress/fixtures/dataWebsol.json').its('[1].opcdispsue').should('eq', valores.opcionesDisponibles )
      }
    });

});


  Then ("Comparo la data web de Sueños contra CRMCloud", () => {
    cy.wait(3500)
    cy.readFile('cypress/fixtures/dataCRM.json').its('[2].puntos').should('eq', valores.puntos )
    cy.readFile('cypress/fixtures/dataCRM.json').its('[2].opcionesDisponibles').should('eq', valores.opcionesDisponibles )
    cy.readFile('cypress/fixtures/dataCRM.json').its('[2].cupones').should('eq', valores.cupon )  
  
  });
