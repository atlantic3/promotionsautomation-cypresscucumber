import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';
  import { credentials, invitado} from '../../support/e2e.js'

  const valoresWebsol = []


  
  Given ("Que Websol esté disponible para revisar la data de promociones", () => {
    cy.visit('https://websol.acity.com.pe/isol/')
  });

  When ("Ingreso  mi usuario y contraseña en Websol", () => {
  
    cy.get('#username').type(credentials.user)
    cy.get('#password').type(credentials.pass)   
  });
  
  
  Then ("Me logueo correctamente en Websol", () => {
    cy.get('.btn').click()
    cy.wait(6000)

  });
  
  Given ("Busco la tarjeta del invitado en Websol",()=>{
    cy.contains('Buscar Cliente').click()
            
    cy.wait(12000)
    cy.get('#PlayerCard').type(invitado.cuenta)
    cy.wait(1000)
    cy.get('#PlayerCard').type('{enter}')
    cy.wait(35000)
  });

  Then ("Obtengo la data de los sorteos y torneos",()=>{

    let opcdispest
    let opcdispsue
    let premiotop
    let puestotop
    let opcdispovip
    let premiomega
    let puestomega
    for(let i=39; i<42; i+=2){
        cy.get(`:nth-child(${i}) > div`).then(($datawb)=> {
            const datawb = $datawb.text() 
            const puntajeT  = datawb.replace(/(\r\n|\n|\r)/gm, "")


            cy.log("TODOS LOS PUNTAJES: ", puntajeT)
            let arregloData = datawb.split(/(\d+(?!\.))/)
            cy.log("SEPARADO: ", arregloData)
            
          
            let textoEstelar
            arregloData.forEach(elemento => {
                if (elemento.match(/.*SORTEO ESTELAR.*/)){
                    textoEstelar = elemento.match(/.*SORTEO ESTELAR.*/)
                    cy.log("INDEX ESTELAR: ",textoEstelar)
                    let dataEstelar = arregloData.indexOf(textoEstelar[0])
                
                    cy.log("DATA ESTELAR: ",dataEstelar)
                
                    opcdispest= arregloData[dataEstelar+1]
                    cy.log("opcdispest",opcdispest)
                }
              
            })
           

            let textoSuenos
            arregloData.forEach(elemento => {
                if (elemento.match(/.*SORTEO SUEÑOS.*/)){
                    textoSuenos = elemento.match(/.*SORTEO SUEÑOS.*/)
                    cy.log("INDEX SUEÑOS: ",textoSuenos)
                    let dataSuenos = arregloData.indexOf(textoSuenos[0])
                
                    cy.log("DATA SUEÑOS: ",dataSuenos)

                    opcdispsue= arregloData[dataSuenos+1]
                    cy.log("opcdispsue",opcdispsue)

                }
            })
        

           
            let textoTop 
            arregloData.forEach(elemento => {
                if (elemento.match(/.*TOP ATLANTIC.*/)){
                    textoTop = elemento.match(/.*TOP ATLANTIC.*/)
                    cy.log("INDEX TOPB: ",textoTop)

                    let dataTop = arregloData.indexOf(textoTop[0])
                
                    cy.log("INDEX TOPA: ",dataTop)

                    premiotop= arregloData[dataTop+1]
                    cy.log("premiotop",premiotop)
                    puestotop= arregloData[dataTop+3]
                    cy.log("puesttop",puestotop)

                }
            })


            let textoVip
            arregloData.forEach(elemento => {
                if (elemento.match(/.*SORTEO VIP.*/)){
                    textoVip = elemento.match(/.*SORTEO VIP.*/)
                    cy.log("INDEX VIP: ",textoVip)

                    let dataVip = arregloData.indexOf(textoVip[0])
                
                    cy.log("DATA VIP: ",dataVip)
                    opcdispovip= arregloData[dataVip+1]
                    cy.log("opcdispovip",opcdispovip)

                }
                
            })
            

            let textoMega 
            arregloData.forEach(elemento => {
                if (elemento.match(/.*MEGA TORNEO.*/)){
                    textoMega = elemento.match(/.*MEGA TORNEO.*/)
                    cy.log("INDEX MEGA: ",textoMega)

                    let dataMega = arregloData.indexOf(textoMega[0])
                
                    cy.log("DATA MEGA: ",dataMega)

                    premiomega= arregloData[dataMega+1]
                    cy.log("premiomega",premiomega)
                    puestomega= arregloData[dataMega+3]
                    cy.log("puestomega",puestomega)
                }

            })

            var datawebsol1 = {opcdispest, opcdispsue,premiotop, puestotop, opcdispovip, premiomega,puestomega}

            valoresWebsol.push(datawebsol1)

            cy.writeFile('cypress/fixtures/dataWebsol.json', valoresWebsol)
            cy.writeFile('cypress/fixtures/dataWebsol.txt', valoresWebsol)
        } 

        )}
    })

