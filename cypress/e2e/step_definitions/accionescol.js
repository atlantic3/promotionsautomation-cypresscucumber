import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';


When ("Al dar click en el menu hamburguesa", () => {
    cy.get('.header_clmc-header-left__n7KNj > .MuiButtonBase-root').click()
});


Then ("Muestra toda las opciones del menu hamburguesa", () => {
    
    cy.get('[href="/casino-online/tragamonedas"] > .marginItemMenu')
  
    cy.get('[href="/casino-online/tragamonedas"] > .marginItemMenu').should('exist').and('be.visible');

    cy.get('.normal-link-login2').should('exist').and('be.visible');

    cy.get('[href="/casino-online/juegos-de-mesa"] > .normal-link').should('exist').and('be.visible');

    cy.get('.normal-link-login3').should('exist').and('be.visible');

    cy.get('.apuestas-link').should('exist').and('be.visible');


  });

//CLICK MAQUINAS
Then ("Ingreso al HOME MAQUINAS", () => {
  cy.visit("https://www.casinoatlanticcity.com/casino-online/tragamonedas");

});

//CLICK MESAS
Then ("Ingreso al HOME MESAS", () => {
  cy.visit("https://www.casinoatlanticcity.com/casino-online/juegos-de-mesa");
});

