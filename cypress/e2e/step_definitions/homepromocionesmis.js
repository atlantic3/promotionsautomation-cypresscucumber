import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';
  


Given ("Que la web este disponible para validar el home mis promociones", () => {
    cy.visit("https://www.casinoatlanticcity.com/casino-online/promociones");
  });

  

Then ("ingreso al home de promociones", () => {
    cy.visit("https://www.casinoatlanticcity.com/casino-online/promociones");
});

Then ("valido el titulo mis promociones activas", () => {
    cy.contains("Mis promociones activas")
    cy.wait(6000)    
  });

Then ("valido el titulo top atlantic", () => {
    cy.contains("Top Atlantic")   
});

Then ("valido el titulo sorteo de tus sueños", () => {
    cy.contains("Sorteo de tus Sueños")
});

Then ("valido el titulo sorteo estelar", () => {
    cy.contains("Sorteo Estelar")
});
Then ("valido el titulo sorteo vip royal", () => {
    cy.contains("Atlantic Vip Royal")
});
Then ("valido el titulo atlantic delivery", () => {
    cy.contains("Delivery de la Semana")
});
Then ("valido el titulo torneo de mesas", () => {
    cy.contains("Torneo de Mesas")
});
Then ("valido el titulo mega torneo", () => {
    cy.contains("Mega Torneo Atlantic")
});
Then ("valido el titulo winner de winners", () => {
    cy.contains("Torneo de Cuotas")
});
Then ("valido el titulo torneo de cuotas", () => {
    cy.contains("Winner de Winners")
});