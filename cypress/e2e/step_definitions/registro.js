import {
    Given,
    When,
    Then,
    And,
  } 
  from "@badeball/cypress-cucumber-preprocessor";
  import 'cypress-wait-until';
  import { credentials, invitado, valores} from '../../support/e2e.js' 
  
  
Given ("que la web de registro esté disponible", () => {
    cy.visit("https://www.casinoatlanticcity.com/registro");
  });
  
Then ("valido que los elementos del paso 1 estén disponibles", () => {
    cy.get('#firstname')
    cy.get('[name="paternal_lastname"]')
    cy.get('[name="maternal_lastname"]')
    cy.get('[name="email"]')
    cy.get('#mui-component-select-national_id_type')
    cy.get('[name="national_id"]')
    cy.get('[type="tel"]')
    cy.get('.clmc-mt20 > .MuiButtonBase-root > .PrivateSwitchBase-input')
    cy.get('.MuiFormControlLabel-root.clmc-mt16 > .MuiButtonBase-root > .PrivateSwitchBase-input')
    cy.get('#btnNextS1')

  });

  Then ("ingreso nombres y apellidos", () => {
    cy.get('#firstname').type('Testregistroaut')
    cy.get('[name="paternal_lastname"]').type('Testregistroaut')
    cy.get('[name="maternal_lastname"]').type('Testregistroaut')

  });


  Then ("ingreso email", () => {
    cy.get('[name="email"]').type('testregistroaut@test.com')
  
  });

  Then ("ingreso tipo de documento y documento", () => {
    cy.get('#mui-component-select-national_id_type').click()
    cy.get('[data-value="PASAPORTE"]').click()
    cy.get('#mui-component-select-national_id_type').click()
    cy.get('[data-value="EXTRANJERIA"]').click()
    cy.get('#mui-component-select-national_id_type').click()
    cy.get('[data-value="DNI"]').click()
    cy.get('[name="national_id"]').type('91232123')
  });


  Then ("ingreso celular", () => {
    cy.get('[type="tel"]').type('921313123')
    
  });

  Then ("marco los recuadros de condiciones y privacidad", () => {
    cy.get('.clmc-mt20 > .MuiButtonBase-root > .PrivateSwitchBase-input').click()
    cy.get('.MuiFormControlLabel-root.clmc-mt16 > .MuiButtonBase-root > .PrivateSwitchBase-input').click()
  });

  Then ("presiono el botón siguiente", () => {
    cy.get('#btnNextS1').click()
  });


  Then ("valido que los elementos del paso 2 estén disponibles", () => {

    cy.get('[name="alias"]')
    cy.get('[name="password"]')
    cy.get('#mui-component-select-currency')
    cy.get(':nth-child(1) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN')
    cy.get(':nth-child(2) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN')
    cy.get(':nth-child(3) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN')
  });

  
  Then ("ingreso mi alias", () => {

    cy.get('[name="alias"]').type('Testregaut0001')
  
  });

  Then ("ingreso mi contraseña", () => {

    cy.get('[name="password"]').type('Testregistroaut01')

 
  });


  Then ("cambio la moneda", () => {

    cy.get('#mui-component-select-currency').click()
    cy.get('[data-value="USD"]').click()
    cy.get('#mui-component-select-currency').click()
    cy.get('[data-value="PEN"]').click()

  });

  Then ("selecciono los tipos de bonos", () => {

    cy.get(':nth-child(1) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN').click()
    cy.get(':nth-child(2) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN').click()
    cy.get(':nth-child(3) > .styles_promotionContainer__ho5J9 > .styles_promotionSummary__CvsVN').click()
    

  });


 

  