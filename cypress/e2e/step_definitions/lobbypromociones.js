import {
  Given,
  When,
  Then,
  And,
} 
from "@badeball/cypress-cucumber-preprocessor";
import 'cypress-wait-until';




Given ("Que la web esté disponible para validar el lobby de promociones", () => {
  cy.visit("https://www.casinoatlanticcity.com/casino-online");
});

When ("Al dar click en el menu", () => {
  
  cy.get('.LoginAsideHeaderButtons_themed-login-buttons__Wi6TW > .styles_primaryButton__02bqI').click({ force: true })    
});


Then ("Ingreso  mi usuario y contraseña", () => {
  cy.get('#user').type("Testcalimaco50")
  cy.get('#password').type("Testcalimaco50")
});

Then ("Me logueo correctamente", () => {
  cy.get('.clmc-dark-form > .clmc-btn-primary').click()
  cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
  cy.wait(5000)
});

Then ("Encuentro el titulo tus promociones", () => {
  cy.wait(2000); 
  cy.contains("Tus promociones")
  
});


Given("Encuentro el titulo de sorteo estelar", () => {
  
  cy.contains("Sorteo Estelar")   
});

Then ("Encuentro la descripcion de sorteo estelar", () => {
  
  cy.contains("Repartimos más de S/170,000 en premios por Navidad")
});

Given("Encuentro el titulo de mega torneo atlantic", () => {
  cy.wait(2000)
  cy.contains("Mega Torneo Atlantic")
});

Then ("Encuentro la descripcion de mega torneo atlantic", () => {
  cy.wait(2000)
  cy.contains("Más de S/125,00 en premios al mes")
});

Given("Encuentro el titulo de top atlantic", () => {
  cy.wait(2000)
  cy.contains("Top Atlantic")
});

Then ("Encuentro la descripcion de top atlantic", () => {
  cy.wait(2000)
  cy.contains("Repartimos más de S/365,000 en premios mensuales")
});

Given("Encuentro el titulo de sorteo de tus sueños", () => {
  cy.wait(2000)
  cy.contains("Sorteo de tus Sueños")
});

Then ("Encuentro la descripcion de sorteo de tus sueños", () => {
  cy.wait(2000)
  cy.contains("Sorteamos S/175,000 en premios por Navidad")  
});

Given("Encuentro el titulo de atlantic vip royal", () => {
  cy.wait(2000)
  cy.contains("Atlantic VIP Royal")
});

Then ("Encuentro la descripcion de atlantic vip royal", () => {
  cy.wait(2000)
  cy.contains("Sorteamos S/160,000 en premios este mes de Navidad")
});

Given("Encuentro el titulo de drops and wins", () => {
  cy.wait(2000)
  cy.contains("DROPS AND WINS")
});

Then ("Encuentro la descripcion de drops and wins", () => {
  cy.wait(2000)
  cy.contains("S/2,000,000 en premios")
});

Given("Encuentro el titulo de torneo de cuotas", () => {
  cy.wait(2000)
  cy.contains("Torneo de Cuotas")
});

Then ("Encuentro la descripcion de torneo de cuotas", () => {
  cy.wait(2000)
  cy.contains("Más de S/50,000 en premios semanales")
});

Given("Encuentro el titulo de torneo winner de winners", () => {
  cy.wait(2000)
  cy.contains("TORNEO WINNER DE WINNERS")
});

Then ("Encuentro la descripcion de torneo winner de winners", () => {
  cy.wait(2000)
  cy.contains("Cada semana más de S/10,000 en premios")
});

Given("Encuentro el titulo de pago anticipado", () => {
  cy.wait(2000)
  cy.contains("Pago Anticipado")
});

Then ("Encuentro la descripcion de pago anticipado", () => {
  cy.wait(2000)
  cy.contains("2 goles de diferencia y ya ganaste")
});

Given("Encuentro el titulo de torneo de mesas", () => {
  cy.wait(2000)
  cy.contains("Torneo de mesas")
});

Then ("Encuentro la descripcion de torneo de mesas", () => {
  cy.wait(2000)
  cy.contains("Más de S/100,000 en premios en diciembre")
});