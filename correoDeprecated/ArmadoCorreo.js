
var fs = require("fs");
const correo = 'correo.html'
let resultado

fs.stat('resultado.txt', function(err) {
  if (err == null) {
    console.log("El archivo existe");
  } else if (err.code == 'ENOENT') {
    console.log("el archivo no existe");
  } else {
    console.log(err); // ocurrió algún error
  }
})



// Nombre del archivo
const archivo = 'resultado.txt';

// Lee el contenido del archivo
fs.readFile(archivo, 'utf8', (err, data) => {
    if (err) {
        console.error('Error al leer el archivo:', err);
        return;
    }

    // Divide el contenido en párrafos
    const parrafos = data.split('\n'); // Cambia '\n\n' por el delimitador correcto

    console.log(parrafos)
    // Elimina un párrafo (por ejemplo, el segundo)
    parrafos.splice(1, 13); // Elimina el segundo párrafo
    // Filtra las líneas que no contienen la palabra "eliminarPalabra"

    const lineasFiltradas = parrafos
    .filter(linea => !linea.includes('Warning'))
    .filter(linea => !linea.includes('Your project has set the configuration'))
    .filter(linea => !linea.includes('This error will not'))
    .filter(linea => !linea.includes('Your project has set the configuration'))
    .filter(linea => !linea.includes('Running'))
    .filter(linea => !linea.includes('TimeoutError'))
    .filter(linea => !linea.includes('at '))
    .filter(linea => !linea.includes('Screenshots'))
    .filter(linea => !linea.includes('Video'))
    .filter(linea => !linea.includes('Results'))
    .filter(linea => !linea.includes('This option will not have an effect in Firefox'))
    .filter(linea => !linea.includes('Timed out retrying'))
  

    // Reconstruye el contenido modificado
    const nuevoContenido = lineasFiltradas.join('\n').replace(/\n\n+/g, '\n\n'); // Reemplaza múltiples saltos de línea por uno solo;;

    // Guarda los cambios en el archivo
    fs.writeFile(archivo, nuevoContenido, (err) => {
        if (err) {
            console.error('Error al escribir en el archivo:', err);
        } else {
            console.log('Cambios guardados correctamente.');
        }
    });

    

     if( lineasFiltradas.filter(linea => linea.includes('failed'))>0)
     {
        resultado ="Tests fallidos, revisar resultados"

     }else {
        resultado ="Tests pasados exitosamente"

     }

      // Convierte el contenido de texto a HTML
      const contenidoHtml = `<html><body><pre>RESULTADOS CHECKLIST PROMOCIONES<br> ${resultado} <br> </pre></body></html>`;

      // Guarda el contenido HTML en un archivo
      fs.writeFile(correo, contenidoHtml, 'utf8', (err) => {
          if (err) {
              console.error('Error al escribir en el archivo HTML:', err);
          } else {
              console.log('Archivo HTML generado correctamente.');
          }
      });

});

