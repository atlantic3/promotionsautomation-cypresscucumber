const Discord = require('discord.js');
const fs = require('fs');

async function enviarCapturaDiscord(rutaArchivo, token, channelId) {
  // Importa los Intents desde discord.js
  const { Client, Intents } = require('discord.js');

  // Define los intents que necesitas
  const intents = new Intents([
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    // Agrega aquí cualquier otro intent que necesites
  ]);

  // Crea un nuevo cliente de Discord con los intents proporcionados
  const client = new Discord.Client({ intents });

  // Espera a que el cliente esté listo
  await client.login(token);

  // Lee el archivo de la captura de pantalla
  const archivo = fs.readFileSync(rutaArchivo);

  // Crea un nuevo mensaje de Discord con la captura de pantalla adjunta
  const mensaje = {
    files: [{
      attachment: archivo,
      name: 'screenshot.jpg'
    }]
  };

  try {
    // Obtiene el canal de Discord
    const canal = await client.channels.fetch(channelId);

    // Envia el mensaje al canal de Discord
    await canal.send('¡Checklist reset!', mensaje);
    
    console.log('Mensaje enviado correctamente al canal de Discord.');
  } catch (error) {
    console.error('Error al enviar el mensaje al canal de Discord:', error);
  } finally {
    // Cierra la conexión del cliente de Discord
    await client.destroy();
  }
}

// Usa la función para enviar la captura de pantalla
enviarCapturaDiscord('/screenshot.jpg', 'MTE0ODYxNzM0MTQ0NjM4OTc4Nw.GnTWlz.Kti7Tfc5DQzYzHf5N8TsoESQI0LWx9zJzp7r-E', '1197546191337033748');

//enviarCapturaDiscord('/screenshot.jpg', 'MTE0ODYxNzM0MTQ0NjM4OTc4Nw.GnTWlz.Kti7Tfc5DQzYzHf5N8TsoESQI0LWx9zJzp7r-E', '1197546191337033748');
